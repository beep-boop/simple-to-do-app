# window.py
#
# Copyright 2022 oEfterdal
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk, Gio, GObject, Pango
from .todo_data import TodoObject

@Gtk.Template(resource_path='/no/efterdal/todo/ui/todo_row.ui')
class TodoRow(Gtk.Box):
    __gtype_name__ = 'TodoRow'

    completed_button = Gtk.Template.Child('completed_button')
    content_label = Gtk.Template.Child('content_label')

    # This is not needed since we have setup bindings.
    # @Gtk.Template.Callback()
    # def on_completed_button(self, *args):
    #     print("YOYOYOY")

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        # Set todo-row as the css node name
        self.set_css_name("todo-row")
        

@Gtk.Template(resource_path='/no/efterdal/todo/ui/window.ui')
class TodoWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'TodoWindow'

    # Get widgets from template
    entry = Gtk.Template.Child('entry')
    list_view = Gtk.Template.Child('list_view')

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        
        
        # Setup the data store for Todo items and pass in a custom object
        self.model = Gio.ListStore.new(TodoObject)
        # Wrap the model with selection and pass it to the listview
        self.list_view.set_model(Gtk.NoSelection.new(self.model))

        # Factory setup
        self.factory = Gtk.SignalListItemFactory.new()
        
        # Binding Factory to 
        self.list_view.set_factory(self.factory)
        
        self.factory.connect("setup", self.on_factory_setup)
        self.factory.connect("bind", self.on_factory_bind)
        self.factory.connect("unbind", self.on_factory_unbind)

    def on_factory_setup(self, factory, item):
        # Create an instance of TodoRow
        self.todo_row = TodoRow()
        # Adding the TodoRow Instance to the list_view
        item.set_child(self.todo_row)

    def on_factory_bind(self, factory, item):
        # Getting the todo_object from list_view
        self.todo_object = item.get_item()
        # Getting the todo_row from list_view
        self.todo_row = item.get_child()
        # Binding todo_object.completed -> todo_row.completed_button
        self.todo_object.bind_property("completed", self.todo_row.completed_button, "active", GObject.BindingFlags.SYNC_CREATE or
                                                                  GObject.BindingFlags.BIDIRECTIONAL)
        # Binding the todo_object.content -> todo_row.content_label.label
        self.todo_object.bind_property("content", self.todo_row.content_label, "label", GObject.BindingFlags.SYNC_CREATE)


    def on_factory_unbind(self, factory, item):
        # Getting the todo_row from list_view
        self.todo_row = item.get_child()
        # Unbinding the row from the list_view
        self.todo_row.unbind()
        print(" Todo row unbinded")

        
    @Gtk.Template.Callback()
    def on_activate(self, entry):
        # Gtk.Entry emits a signal when user hit enter/return
        # Signal: "activate" Handler: "on_activate"
        if entry.props.buffer.get_length() > 0:
            # Check if there actully any data in the buffer
            self.item = TodoObject()
            # Creates and Todo Item object
            self.item.content = entry.get_text()
            # Appending the content from buffer to a Gtk.ListStore
            self.model.append(self.item)
            # Set the Gtk.Entry to a clean buffer
            entry.props.buffer = None


class AboutDialog(Gtk.AboutDialog):

    def __init__(self, parent):
        Gtk.AboutDialog.__init__(self)
        self.props.program_name = 'todo'
        self.props.version = "0.1.0"
        self.props.authors = ['oEfterdal']
        self.props.copyright = '2022 oEfterdal'
        self.props.logo_icon_name = 'no.efterdal.todo'
        self.props.modal = True
        self.set_transient_for(parent)
