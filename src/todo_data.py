from gi.repository import GObject, GLib


class TodoObject(GObject.GObject):
    """ TodoObject """
    completed = GObject.Property(type=bool, default=False)
    content = GObject.Property(type=str)

    def __init__(self):
        GObject.GObject.__init__(self)

