# Simple To-Do App

Python3 + GTK4 implementation of gtk4-rs [Building a Simple To-Do App](https://gtk-rs.org/gtk4-rs/stable/latest/book/todo_app_1.html) tutorial.

* Gnome Builder
* GTK4
* Python3
